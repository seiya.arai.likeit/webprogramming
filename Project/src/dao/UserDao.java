package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.User;

public class UserDao {

	public User findByLoginInfo(String loginId, String password) {
		Connection conn = null;
		try {
			//データベースへ接続
			conn = DBManager.getConnection();

			//TODO ここに処理を書いていく
			//確認済みのSQL
			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

			String result = encryption(password);

			//SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, result);
			ResultSet rs = pStmt.executeQuery();

			//ログイン失敗時の処理
			if (!rs.next()) {
				return null;
			}

			//ログイン成功時の処理
			int id = rs.getInt("id");
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			return new User(id, loginIdData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			//データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public List<User> findAll() {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			//データベースへ接続
			conn = DBManager.getConnection();
			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "SELECT * FROM user WHERE id != 1";

			// SELECTを実行し、結果表を取得;
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_Date");
				User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

				userList.add(user);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;

				}
			}
		}
		return userList;
	}

	public List<User> findSearch(String loginIdP, String nameP, String startDate, String endDate) {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			//データベースへ接続
			conn = DBManager.getConnection();
			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "SELECT * FROM user WHERE id != 1";

			if(!loginIdP.equals("")) {
				sql += " AND login_id =  '" + loginIdP + "'";
			}

			if(!nameP.equals("")) {
				sql += " AND name LIKE '%"+ nameP +"%'";
			}

			if(!startDate.equals("")) {
				sql += " AND birth_date >= '" + startDate + "'";
			}

			if(!endDate.equals("")) {
				sql += " AND birth_date <= '" + endDate + "'";
			}

			// SELECTを実行し、結果表を取得;
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_Date");
				User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

				userList.add(user);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;

				}
			}
		}
		return userList;
	}

	public void userAdd(String loginId, String name, String password, String birthDate) {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();
			String sql = "INSERT INTO user(login_id ,name , password , birth_date,create_date , update_date) VALUES (? , ? , ? , ?,NOW(),NOW())";

			String result = encryption(password);

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, name);
			pStmt.setString(3, result);
			pStmt.setString(4, birthDate);
			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public User userDetail(String id) {
		Connection conn = null;
		try {
			//データベースへ接続
			conn = DBManager.getConnection();

			//TODO ここに処理を書いていく
			//確認済みのSQL
			String sql = "SELECT * FROM user WHERE id=?";

			//SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			//ログイン失敗時の処理
			if (!rs.next()) {
				return null;
			}

			//詳細ボタン押下時の処理
			int idData = rs.getInt("id");
			String loginId = rs.getString("login_id");
			String name = rs.getString("name");
			Date birthDate = rs.getDate("birth_date");
			String password = rs.getString("password");
			String createDate = rs.getString("create_date");
			String updateDate = rs.getString("update_date");
			return new User(idData, loginId, name, birthDate, password, createDate, updateDate);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			//データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public void userUpdate(String name, String birthDate, String password, String id) {
		Connection conn = null;
		try {
			//データベースへ接続
			conn = DBManager.getConnection();

			//TODO ここに処理を書いていく
			//確認済みのSQL
			String sql = "UPDATE user SET name=? ,birth_date=? ,password=? WHERE id=?";

			String result = encryption(password);

			//UPDATEを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, name);
			pStmt.setString(2, birthDate);
			pStmt.setString(3, result);
			pStmt.setString(4, id);
			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void userDelete(String id) {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			String sql = "DELETE FROM user WHERE id=?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		}
	}

	public String userErr(String loginId) {
		Connection conn = null;
		try {
			//データベースへ接続
			conn = DBManager.getConnection();

			//TODO ここに処理を書いていく
			//確認済みのSQL
			String sql = "SELECT * FROM user WHERE login_id=?";

			//SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			ResultSet rs = pStmt.executeQuery();

			//ログイン失敗時の処理
			if (!rs.next()) {
				return null;
			}

			String loginIdData = rs.getString("login_id");
			return loginIdData;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public void userPass(String name, String birthDate, String id) {
		Connection conn = null;
		try {
			//データベースへ接続
			conn = DBManager.getConnection();

			//TODO ここに処理を書いていく
			//確認済みのSQL
			String sql = "UPDATE user SET name=? ,birth_date=? WHERE id=?";

			//UPDATEを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, name);
			pStmt.setString(2, birthDate);
			pStmt.setString(3, id);
			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public String encryption(String password) {
		try {
		String source = password;
		Charset charset = StandardCharsets.UTF_8;
		String algorithm = "MD5";

			byte[] bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
			String result = DatatypeConverter.printHexBinary(bytes);
			System.out.println(result);

			return result;
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		}


	}
}
