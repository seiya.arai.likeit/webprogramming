<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ユーザ削除確認</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">

<link href="css/original/common.css" rel="stylesheet">

</head>

<body>


	<header>
		<nav class="navbar navbar-dark bg-dark navbar-expand  flex-md-row">
			<ul class="navbar-nav navbar-dark bg-dark flex-row mr-auto">
				<li class="nav-item active"><a class="nav-link"
					href="login.jsp">ユーザ管理システム</a></li>
			</ul>
			<ul class="navbar-nav flex-row">
				<li class="nav-item"><a class="nav-link" href="#">${userInfo.name} さん</a>
				</li>
				<li class="nav-item"><a class="btn btn-primary"
					href="UserLogoutServlet">ログアウト</a></li>
			</ul>
		</nav>
	</header>

	<h1>ユーザ削除確認</h1>
	<div class="form-group row">
		<h5>ログインID：</h5>
		<p>${user.loginId}</p>
	</div>
	<h5>を本当に削除してよろしいでしょうか</h5>

	<form action="UserDeleteServlet" method="post">
	<input type="hidden" class="form-control" id="inputPassword" name="id" value="${user.id}">
		<div class="form-group row">
			<div class="col-sm-5">
				<a class="btn btn-primary center-block form-submit" href="UserListServlet">キャンセル</a>
			</div>
			<div class="col-sm-5">
				<button type="submit" value="検索"
					class="btn btn-primary center-block form-submit">OK</button>
			</div>
		</div>
	</form>
</body>
</html>
