<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ユーザ情報更新</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link href="css/original/common.css" rel="stylesheet">

</head>

<body>


    <header>
        <nav class="navbar navbar-dark bg-dark navbar-expand  flex-md-row">
            <ul class="navbar-nav navbar-dark bg-dark flex-row mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="login.jsp">ユーザ管理システム</a>
                </li>
            </ul>
            <ul class="navbar-nav flex-row">
                <li class="nav-item">
                    <a class="nav-link" href="#">${userInfo.name} さん</a>
                </li>
                <li class="nav-item">
                    <a class="btn btn-primary" href="UserLogoutServlet">ログアウト</a>
                </li>
            </ul>
        </nav>
    </header>

    <h1>ユーザ情報更新</h1>
    <c:if test="${errMsg != null}">
			<div class="alert alert-danger" role="alert">${errMsg}</div>
		</c:if>
    <form action="UserUpdateServlet" method="post">
    <input type="hidden" class="form-control" id="inputPassword" name="id" value="${user.id}">
        <div class="form-group row">
            <label for="staticLoginId" class="col-sm-2 col-form-label">ログインID</label>
            <div class="col-sm-10">
                <td>${user.loginId}</td>
            </div>
        </div>

        <div class="form-group row">
            <label for="inputPassword" class="col-sm-2 col-form-label">パスワード</label>
            <div class="col-sm-10">
                <input type="password" class="form-control" id="inputPassword" name="password1">
            </div>
        </div>

        <div class="form-group row">
            <label for="inputPassword" class="col-sm-2 col-form-label">パスワード（確認）</label>
            <div class="col-sm-10">
                <input type="password" class="form-control" id="inputPassword" name="password2">
            </div>
        </div>

        <div class="form-group row">
            <label for="inputUserName" class="col-sm-2 col-form-label">ユーザ名</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="inputUserName" name="name" value="${user.name}">
            </div>
        </div>

        <div class="form-group row">
            <label for="inputDate" class="col-sm-2 col-form-label">生年月日</label>
            <div class="col-sm-10">
                <input type="date" class="form-control" id="inputDate" name="birthDate" value="${user.birthDate}">
            </div>
        </div>

        <div>
            <button type="submit" value="更新" class="btn btn-primary center-block form-submit">更新</button>
        </div>

        <div class="col-xs-4">
            <a href="UserListServlet">戻る</a>
        </div>
    </form>
</body></html>
