<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ユーザ一覧</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">

<link href="css/original/common.css" rel="stylesheet">

</head>

<body>



	<header>
		<nav class="navbar navbar-dark bg-dark navbar-expand  flex-md-row">
			<ul class="navbar-nav navbar-dark bg-dark flex-row mr-auto">
				<li class="nav-item active"><a class="nav-link"
					href="login.jsp">ユーザ管理システム</a></li>
			</ul>
			<ul class="navbar-nav flex-row">
				<li class="nav-item"><a class="nav-link" href="#">${userInfo.name}
						さん</a></li>
				<li class="nav-item"><a class="btn btn-primary"
					href="UserLogoutServlet">ログアウト</a></li>
			</ul>
		</nav>
	</header>

	<h1>ユーザ一覧</h1>

	<div class="col-xs-4">
		<a href="UserAddServlet">新規登録</a>
	</div>

	<form action="UserListServlet" method="post">
		<div class="form-group row">
			<label for="inputLoginId" class="col-sm-2 col-form-label">ログインID</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id="inputLoginId"
					name="loginId">
			</div>
		</div>

		<div class="form-group row">
			<label for="inputUserName" class="col-sm-2 col-form-label">ユーザ名</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id="inputUserName"
					name="name">
			</div>
		</div>

		<div class="form-group row">
			<label for="inputDate" class="col-sm-2 col-form-label">生年月日</label>
			<div class="col-sm-4">
				<input type="date" class="form-control" id="inputDate"
					name="startDate">
			</div>
			<div class="col-sm-1 text-center">~</div>
			<div class="col-sm-4">
				<input type="date" class="form-control" id="inputDate"
					name="endDate">
			</div>
		</div>

		<div>
			<button type="submit" value="検索"
				class="btn btn-primary center-block form-submit">検索</button>
		</div>

	</form>

	<table class="table table-striped">
		<thead>
			<tr>
				<th scope="col">ログインID</th>
				<th scope="col">ユーザ名</th>
				<th scope="col">生年月日</th>
				<th scope="col"></th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="user" items="${userList}">
				<tr>
					<td>${user.loginId}</td>
					<td>${user.name}</td>
					<td>${user.birthDate}</td>
					<!-- TODO 未実装；ログインボタンの表示制御を行う -->
					<c:if test="${userInfo.id == 1}">
						<td><a class="btn btn-primary"
							href="UserDetailServlet?id=${user.id}">詳細</a> <a
							class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
							<a class="btn btn-danger" href="UserDeleteServlet?id=${user.id}">削除</a>
						</td>
					</c:if>
					<c:if test="${userInfo.id != 1}">
						<td><a class="btn btn-primary"
							href="UserDetailServlet?id=${user.id}">詳細</a>
							<c:if test="${userInfo.id == user.id}">
							<a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
							</c:if>
						</td>
					</c:if>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</body>
</html>
