<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ユーザ情報詳細参照</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">

<link href="css/original/common.css" rel="stylesheet">

</head>

<body>


	<header>
		<nav class="navbar navbar-dark bg-dark navbar-expand  flex-md-row">
			<ul class="navbar-nav navbar-dark bg-dark flex-row mr-auto">
				<li class="nav-item active"><a class="nav-link"
					href="login.jsp">ユーザ管理システム</a></li>
			</ul>
			<ul class="navbar-nav flex-row">
				<li class="nav-item"><a class="nav-link" href="#">${userInfo.name}さん</a></li>
				<li class="nav-item"><a class="btn btn-primary"
					href="UserLogoutServlet">ログアウト</a></li>
			</ul>
		</nav>
	</header>

	<h1>ユーザ情報詳細参照</h1>

	<form>
		<div class="form-group row">
			<label for="staticLoginId" class="col-sm-2 col-form-label">ログインID</label>
			<div class="col-sm-10">
				<td>${user.loginId}</td>
			</div>
		</div>

		<div class="form-group row">
			<label for="staticUserName" class="col-sm-2 col-form-label">ユーザ名</label>
			<div class="col-sm-10">
				<td>${user.name}</td>
			</div>
		</div>

		<div class="form-group row">
			<label for="statiDate" class="col-sm-2 col-form-label">生年月日</label>
			<div class="col-sm-10">
				<td>${user.birthDate}</td>
			</div>
		</div>

		<div class="form-group row">
			<label for="statiDate" class="col-sm-2 col-form-label">登録日時</label>
			<div class="col-sm-10">
				<td>${user.createDate}</td>
			</div>
		</div>

		<div class="form-group row">
			<label for="statiDate" class="col-sm-2 col-form-label">更新日時</label>
			<div class="col-sm-10">
				<td>${user.updateDate}</td>
			</div>
		</div>

		<div class="col-xs-4">
			<a href="UserListServlet">戻る</a>
		</div>
	</form>

</body>
</html>
