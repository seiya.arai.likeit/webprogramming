CREATE DATABASE usermanagement DEFAULT CHARACTER SET utf8;
use usermanagement;
CREATE TABLE usermanagement(
id SERIAL PRIMARY KEY UNIQUE NOT NULL AUTO_INCREMENT,
login_id varchar(255) UNIQUE NOT NULL,
name varchar(255) NOT NULL,
birth_date DATE NOT NULL,
password varchar(255) NOT NULL,
create_date DATETIME NOT NULL,
update_date DATETIME NOT NULL
);
INSERT INTO usermanagement(id,login_id,name,birth_date,password,create_date,update_date)VALUES(1,'admin','管理者','1992/07/04','password','2020/07/21',now());